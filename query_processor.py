
class ProccessQuery():
    
    def __init__(self,mysql):
        self.mysql = mysql
        self.conn = mysql.connect()
        self.cursor = self.conn.cursor()
    
    def process_query(self,query):
        try:
            self.cursor.execute(query)
            data = self.cursor.fetchall()
            self.conn.commit()
            return data
        except Exception as e:
            raise e
        finally:
            self.conn.close()