from flask import Flask,request,Response,send_from_directory,render_template,\
                redirect,send_file,send_from_directory,make_response,jsonify, session
from scrapper.main_scrapper import main_caller,new_main_caller
from jinja2 import evalcontextfilter, Markup
import os
import pickle
import uuid
import emailsender
import creditmanager
from flask_babel import Babel, gettext
from flaskext.mysql import MySQL

from query_processor import ProccessQuery
print(os.getcwd())
app = Flask(__name__)
babel = Babel(app)

app.secret_key = '@KJHKJH@!@#!@$67564*&64354'
app.config['SESSION_TYPE'] = 'filesystem'
# try:
#     with open('Userdata.pkl', 'rb') as input:
#         username = pickle.load(input)
# except:
#     username = {"yash":{'credits':30,'password':'pass1234'}}
# 
# username["yash"]["credits"] = 900000000000000000000
# username["yash"]["vericode"] = 'verified'

mysql = MySQL()
 
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'amazonkeyword'
app.config['MYSQL_DATABASE_HOST'] = '127.0.0.1'
#app.config['MYSQL_DATABASE_PORT'] = 3307
mysql.init_app(app)

mailer = emailsender.mailer()
cm = creditmanager.CM(mysql)


@babel.localeselector
def get_locale():
    # if the user has set up the language manually it will be stored in the session,
    # so we use the locale from the user settings
    try:
        language = session['language']
    except KeyError:
        language = None
    if language is not None:
        return language
    if (request.accept_languages.best_match(['zh','en','zh-Hans','zh-CN','zh-HK','zh-Hant'])) in ['zh-Hans','zh-CN','zh-HK','zh-Hant']:
        return 'zh'
    return request.accept_languages.best_match(['en','zh'])

@app.route('/language/<language>')
def set_language(language=None):
    session['language'] = language
    return redirect(request.referrer)

@app.template_filter()
@evalcontextfilter
def generate_string(eval_ctx, localized_value):
    if localized_value is None:
        return ""
    else:
        return Markup("\"" + localized_value + "\"").unescape()

@app.route('/test')
def test():	
    query = 'select email from user'
    pq = ProccessQuery(mysql)
    user_data = pq.process_query(query)
    return 	user_data[0][0]

@app.route('/verify')
def get_verify():
    username = request.args.get('username')
    if username != None:
        return '<html><body><center><h1>Enter your verification code</h1><br><br><br><br><br><br><form action="/verify" method="post"><label><br><br>Username:<input type="text" name="Username" value = "'+username+'" placeholder = "Enter Username Here" size=30><br>verification code:<input type="text" name="verification" placeholder = "Enter verification code Here" size=60></label><button type="submit" value="Submit">Submit</button></form> <p>Did not receive verification email, <a href="sendverify?username='+username+'">resend</a></p></center></body></html>'
    return '<html><body><center><h1>Enter your verification code</h1><br><br><br><br><br><br><form action="/verify" method="post"><label><br><br>Username:<input type="text" name="Username" placeholder = "Enter Username Here" size=30><br>verification code:<input type="text" name="verification" placeholder = "Enter verification code Here" size=60></label><button type="submit" value="Submit">Submit</button></form></center></body></html>'

@app.route('/new/<id>')
def get_new_page(id):
    return render_template('login.html',page=id)

sessids = {}

@app.route('/new/login', methods = ['POST'])
def post_new_login():
    email = request.form.get("username")
    query="select password,credits,bonus_credits,vericode from user where email='%s'"%(email)
    pq = ProccessQuery(mysql)
    user_data = pq.process_query(query)
    print('userdata::'+str(user_data))
    if user_data:
        if user_data[0][0] == request.form.get("password"):
            if user_data[0][3] == 'verified':
                if user_data[0][1] <= 0 and user_data[0][2] <= 0 :
                    return render_template('login.html',page='login',message=gettext('your credit balance is zero'))
                resp = make_response(redirect('/new/introduction'))
                key = uuid.uuid4().hex
                sessids[key]=request.form.get("username")
                resp.set_cookie('sessid',key)
                return resp
            else:
                return render_template('login.html',page='verify')
        else:
            return render_template('login.html',page='login',message=gettext('your password is incorrect'))
    else:
        return render_template('login.html',page='login',message=gettext('username does not exist'))

@app.route('/new/register', methods = ['POST'])
def post_new_register():
    print(request.form.get("username1"))
    print(request.form.get("password1"))
    print(request.form.get("password2"))
    print(request.form.get("ref_id"))
    query = "select email from user where email='%s'"%(request.form.get("username1"))
    pq = ProccessQuery(mysql)
    data = pq.process_query(query)
    if not data: #or request.form.get("username1") is not None:
        if request.form.get("password1") == request.form.get("password2"):
            email = request.form.get("username1")
            password = request.form.get('password1')
            credits = 50
            referral_id= uuid.uuid4().hex[0:5]
            #username[request.form.get('username1')]["bonus_credits"]=0
            #username[request.form.get('username1')]["referral_count"]=0
            redeemed_credits=50
            #username[request.form.get('username1')]["bonus_credits_growth"]=0
            #username[request.form.get('username1')]["referral_count_growth"]=0
            #username[request.form.get('username1')]["redeemed_credits_growth"]=0
            vericode = uuid.uuid4().hex
            #username[request.form.get('username1')]["vericode"] = vericode
            if not request.form.get('ref_id'):
                query= "insert into user(email,password,credits,referral_id,redeemed_credits,vericode)\
                        values ('%s','%s',%s,'%s',%s,'%s');"%(email,password,credits,referral_id,redeemed_credits,vericode)
                print("query-->"+str(query))
                pq = ProccessQuery(mysql)
                pq.process_query(query)
            if request.form.get('ref_id'):
                referred_by = get_user_by_ref_id(request.form.get('ref_id'))
                print("referred_by::"+str(referred_by))
                referrer =None
                if not referred_by is None:
                    referrer = referred_by[0]
                    if referred_by[1]:
                        referral_count =referred_by[1]+1
                    else:
                        referral_count =1
                    referral_count_growth= get_percent_growth(referral_count,1)
                    update_query = "update user set referral_count_growth=%s,referral_count=%s where email='%s'"%(referral_count_growth,referral_count,referred_by[0])
                    pq = ProccessQuery(mysql)
                    pq.process_query(update_query)
                
                query= "insert into user(email,password,credits,referral_id,redeemed_credits,vericode,referred_by)\
                        values ('%s','%s',%s,'%s',%s,'%s','%s');"%(email,password,credits,referral_id,redeemed_credits,vericode,referrer)
                print("query-->"+str(query))
                pq = ProccessQuery(mysql)
                pq.process_query(query)
            mailer.send(request.form.get('username1'),vericode)
            return render_template('login.html',page='verify',username=request.form.get('username1'))
        else:
                return render_template('login.html',page='register', message=gettext('your passwords do not match'))
    else:
        return render_template('login.html',page='register',message=gettext('Username already exists, please login'))

@app.route('/new/verify', methods = ['POST'])
def post_new_verify():
    v = request.form.get('vericode')
    u = request.form.get('username5')
    query = "select vericode from user where email='%s'"%(u)
    pq = ProccessQuery(mysql)
    data = pq.process_query(query)
    if data and data[0][0]!='verified':
        if data[0][0] == v:
            update_query = "update user set vericode='verified' where email='%s'"%(u)
            pq = ProccessQuery(mysql)
            pq.process_query(update_query) 
            #username[u]["vericode"] = 'verified'
            resp = make_response(redirect('/new/user-dashboard'))
            key = uuid.uuid4().hex
            sessids[key]=request.form.get("username5")
            resp.set_cookie('sessid',key)
#             with open("Userdata.pkl",'wb') as output:
#                 pickle.dump(username,output,pickle.HIGHEST_PROTOCOL)
            return resp
        else:
            return render_template('login.html',page='verify', username=u,message=gettext('Invalid verification code'))
    else:
        return render_template('login.html',page='verify', username=u,message=gettext('Invalid verification code or username'))

@app.route('/new/forgot', methods = ['POST'])
def post_new_forgot():
    user = request.form.get('username4')
    query = "select reset_code from user where email='%s'"%(user)
    pq = ProccessQuery(mysql)
    data = pq.process_query(query)
    if data:
        if not data[0][0]:
            reset_code=uuid.uuid4().hex
            update_query="update user set reset_code='%s' where email='%s'"%(reset_code,user)
            pq = ProccessQuery(mysql)
            pq.process_query(update_query)
            mailer.send_reset_mail(user, reset_code)
#             with open("Userdata.pkl",'wb') as output:
#                 pickle.dump(username,output,pickle.HIGHEST_PROTOCOL)
            return render_template('login.html',page='reset',message=gettext("reset code has been sent to your registered email"))
        else:
            mailer.send_reset_mail(user,data[0][0])
            return render_template('login.html',page='reset', username=user,message=gettext("reset code has been resent to your registered email"))
    else:
        return render_template('login.html',page='forgot', message=gettext("username does not exist"))


@app.route('/new/reset', methods = ['POST'])
def post_new_reset():
    p1 = request.form.get('password3')
    p2 = request.form.get('password4')
    rc = request.form.get('reset_code')
    user = request.form.get('username6')
    query = "select reset_code from user where email='%s'"%(user)
    print(query)
    pq = ProccessQuery(mysql)
    data = pq.process_query(query)
    if user is None or p1 is None or p2 is None or rc is None:
        return render_template('login.html',page='reset',username=user,message=gettext('incorrect input. please try again'))
    if not p1 == p2:
        return render_template('login.html',page='reset',username=user,message=gettext('your passwords do not match'))
    if not data[0][0] == rc:
        return render_template('login.html',page='reset',username=user,message=gettext('your reset code is incorrect'))
    #username[user]["password"] = p1
    update_query="update user set password='%s' where email='%s'"%(p1,user)
    pq = ProccessQuery(mysql)
    pq.process_query(update_query)
    resp = make_response(redirect('/new/user-dashboard'))
    key = uuid.uuid4().hex
    sessids[key]=user
    resp.set_cookie('sessid',key)
#     with open("Userdata.pkl",'wb') as output:
#         pickle.dump(username,output,pickle.HIGHEST_PROTOCOL)
    return resp


def get_user_data(user):
    query = "select bonus_credits,referral_count,redeemed_credits,referral_id,bonus_credits_growth,referral_count_growth,\
            redeemed_credits_growth from user where email='%s'"%(user)
    pq = ProccessQuery(mysql)
    user_data = pq.process_query(query)
    data = dict()
    data["credits"]=0
    data["redeemed_credits"]=user_data[0][2] if user_data[0][2] else 0
    data["bonus_credits"]= user_data[0][0] if user_data[0][0] else 0
    data["redeemed_credits_growth"]=user_data[0][6] if user_data[0][6] else 0
    data["bonus_credits_growth"]=user_data[0][4] if user_data[0][4] else 0
    data["referral_count_growth"]=user_data[0][5] if user_data[0][5] else 0
    data["referral_count"]=user_data[0][1] if user_data[0][1] else 0
    data["referral_id"]=user_data[0][3]
    return data
    

@app.route('/new/introduction')
def introduction():
    if request.cookies.get('sessid') in sessids.keys():
        return render_template('introduction.html',user=sessids[request.cookies.get('sessid')] ,data = get_user_data(sessids[request.cookies.get('sessid')]))
    else:
        return redirect('/new/login')
    
@app.route('/new/admin')
def new_admin():
    return render_template('admin_login.html',message='')


@app.route('/new/admin', methods = ['POST'])
def post_new_admin():
    if request.form.get("username") == 'admin' and request.form.get("password") == "!C20180524n":
        resp = make_response(redirect('/new/admin-dashboard'))
        key = uuid.uuid4().hex
        sessids[key]='admin'
        resp.set_cookie('sessid',key)
        return resp
    else:
        return render_template('admin_login.html',message='Incorrect details try again')



@app.route('/new/user-dashboard')
def new_user_dashboard():
    if request.cookies.get('sessid') in sessids.keys():
        import scrapper.ls
        ls = scrapper.ls.linkstore()
        links = ls.get_one_from_table(sessids[request.cookies.get('sessid')])
        return render_template('index.html',user=sessids[request.cookies.get('sessid')] ,data = get_user_data(sessids[request.cookies.get('sessid')]),links=links, lenlinks = len(links))
        #return "you are loggged in as "+sessids[request.cookies.get('sessid')]+", user dashboard will go here <a href=\"logout\">logout</a>"
    else:
        return redirect('/new/login')

@app.route('/new/redeem', methods=['POST'])
def post_redeem():
    if request.cookies.get('sessid') not in sessids.keys():
        return redirect('/new/login')
        #return render_template('redeem.html',user=sessids[request.cookies.get('sessid')] ,data = username[sessids[request.cookies.get('sessid')]])
    code = request.form.get('voucher')
    user = sessids[request.cookies.get('sessid')]
    value = cm.claim(code)
    if value is None:
        return redirect('/new/redeem?d=1002')
    query = "select credits, redeemed_credits,referred_by from user where email='%s'"%(user)
    pq = ProccessQuery(mysql)
    user_data = pq.process_query(query)
    credits= user_data[0][0]+value
    rc = user_data[0][1] if user_data[0][1] else 0
    redeemed_credits_growth=get_percent_growth(rc,value)
    redeemed_credits=rc+value
    update_query="update user set credits=%s,redeemed_credits_growth=%s,redeemed_credits=%s where \
                 email='%s'"%(credits,redeemed_credits_growth,redeemed_credits,user)
    pq = ProccessQuery(mysql)
    pq.process_query(update_query)
    if user_data[0][2]:
        referred_by = user_data[0][2]
        query = "select credits, bonus_credits from user where email='%s'"%(referred_by)
        pq = ProccessQuery(mysql)
        data = pq.process_query(query)
        if data:
            ten_percent = int(value*0.2)
            bonus_credits_growth=get_percent_growth(data[0][1],ten_percent)
            bonus_credits=data[0][1]+ten_percent
            credits=data[0][0]+ten_percent
            update_query="update user set credits=%s,bonus_credits_growth=%s,bonus_credits=%s where \
                 email='%s'"%(credits,bonus_credits_growth,bonus_credits,referred_by)
            pq = ProccessQuery(mysql)
            pq.process_query(update_query)
#     with open("Userdata.pkl",'wb') as output:
#         pickle.dump(username,output,pickle.HIGHEST_PROTOCOL)
    return redirect('/new/redeem?d=1001&v='+str(value))

def get_percent_growth(before,incre):
    try:
        return int((float(incre)/float(before))*100)
    except:
        return 0


@app.route('/new/redeem')
def get_redeem():
    if request.cookies.get('sessid') in sessids.keys():
        if request.args.get('d') is not None:

            d = str(request.args.get('d'))

            if d == '1001':
                return render_template('redeem.html',user=sessids[request.cookies.get('sessid')] ,data = get_user_data(sessids[request.cookies.get('sessid')]),notification='redeem request successful. you have added '+request.args.get('v')+' credits')
            if d == '1002':
                return render_template('redeem.html',user=sessids[request.cookies.get('sessid')] ,data = get_user_data(sessids[request.cookies.get('sessid')]),notification='bad voucher code')

        return render_template('redeem.html',user=sessids[request.cookies.get('sessid')] ,data = get_user_data(sessids[request.cookies.get('sessid')]))
    else:
        return redirect('/new/login')


@app.route('/new/admin-dashboard')
def new_admin_dashboard():
    if request.cookies.get('sessid') in sessids.keys():
        #return "you are loggged in as "+sessids[request.cookies.get('sessid')]+", Admin dashboard will go here <a href=\"logout-admin\">logout</a>"
        user = 'admin'
        data,username = getAdminData()
        return render_template('admin_index.html',user=user,data=data,users=username,ta=data['ta'],i=0)
    else:
        return redirect('/new/admin')

def getAdminData():
    data = dict()
    data["credits"] =0
    data["redeemed_credits"]=0
    data["bonus_credits"]=0
    data["redeemed_credits_growth"]=0
    data["bonus_credits_growth"]=0
    data["referral_count_growth"]=0
    data["referral_count"]=0
    data["verified_count"]=0
    query = "select credits,redeemed_credits,redeemed_credits_growth,bonus_credits,bonus_credits_growth,referral_count,referral_count_growth,\
             vericode,email,referral_id from user"
    pq = ProccessQuery(mysql)
    user_data = pq.process_query(query)
    verified_count=0
    username={}
    user_num = len(user_data)
    for i in range(user_num):
        credits = user_data[i][0] if user_data[i][0] else 0
        redeemed_credits = user_data[i][1] if user_data[i][1] else 0
        bonus_credits=user_data[i][3] if user_data[i][3] else 0
        redeemed_credits_growth =user_data[i][2] if user_data[i][2] else 0
        bonus_credits_growth=user_data[i][4] if user_data[i][4] else 0
        referral_count_growth =user_data[i][6] if user_data[i][6] else 0
        referral_count = user_data[i][5] if user_data[i][5] else 0
        vericode = user_data[i][7]
        referral_id = user_data[i][9]
        usernam_temp={
            'credits': credits,
            "redeemed_credits":redeemed_credits,
            "bonus_credits":bonus_credits,
            "redeemed_credits_growth":redeemed_credits_growth,
            "bonus_credits_growth":bonus_credits_growth,
            "referral_count_growth":referral_count_growth,
            "referral_count":referral_count,
            "vericode":vericode,
            "referral_id":referral_id
            }
        username[user_data[i][8]] = usernam_temp
        data["credits"] += credits
        data["redeemed_credits"]+=redeemed_credits
        data["bonus_credits"]+=bonus_credits
        data["redeemed_credits_growth"]+=redeemed_credits_growth
        data["bonus_credits_growth"]+=bonus_credits_growth
        data["referral_count_growth"]+=referral_count_growth
        data["referral_count"]+=referral_count
        if user_data[i][7] == 'verified':
            data["verified_count"] +=1
        
#     for user in username:
#         for key in username[user]:
#             if key in ['credits','redeemed_credits','redeemed_credits_growth','bonus_credits','bonus_credits_growth','referral_count','referral_count_growth']:
#                 data[key]+=username[user][key]
#         if username[user]['vericode'] == 'verified':
#             data["verified_count"]+=1
    if user_num!=0:
        data["redeemed_credits_growth"]=int(float(data["redeemed_credits_growth"])/float(len(user_data)))
        data["bonus_credits_growth"]=int(float(data["bonus_credits_growth"])/float(len(user_data)))
        data["referral_count_growth"]=int(float(data["referral_count_growth"])/float(len(user_data)))
    data["ta"] = len(user_data)
    return data,username


@app.route('/new/vouchers')
def new_admin_vouchers():
    if request.cookies.get('sessid') in sessids.keys():
        #return "you are loggged in as "+sessids[request.cookies.get('sessid')]+", Admin dashboard will go here <a href=\"logout-admin\">logout</a>"
        user = 'admin'
        data, username = getAdminData()
        cm_new = creditmanager.CM(mysql)
        return render_template('admin_vouchers.html',user=user,users=username,data=data,codes=cm_new.vouchers(),ta=data['ta'])
    else:
        return redirect('/new/admin')


@app.route('/new/logout')
def new_logout():
    deleted_session = sessids.pop(request.cookies.get('sessid'),None)
    return redirect('/new/login')

@app.route('/new/logout-admin')
def new_logout_admin():
    deleted_session = sessids.pop(request.cookies.get('sessid'),None)
    return redirect('/new/admin')

@app.route('/new/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js', path)

@app.route('/new/css/<path:path>')
def send_css(path):
    return send_from_directory('static/css', path)

@app.route('/new/img/<path:path>')
def send_img(path):
    return send_from_directory('static/img', path)

@app.route('/new/fonts/<path:path>')
def send_font(path):
    return send_from_directory('static/fonts', path)

def reduce_credits(user):
    query = "select redeemed_credits, credits, bonus_credits from user where email='%s'"%(user)
    pq = ProccessQuery(mysql)
    user_data = pq.process_query(query)
    if user_data[0][0] > 0:
        credits = user_data[0][1] - 1
        redeemed_credits = user_data[0][0] - 1
        update_query = "update user set credits=%s, redeemed_credits=%s where email='%s'"%(credits, redeemed_credits, user)
        #username[user]["credits"] -= 1
        #username[user]["redeemed_credits"] -= 1
    else:
        credits = user_data[0][1] - 1
        bonus_credits = user_data[0][2] - 1
        update_query = "update user set credits=%s, bonus_credits=%s where email='%s'"%(credits, bonus_credits, user)
    pq = ProccessQuery(mysql)
    pq.process_query(update_query)
        #username[user]["credits"] -= 1
        #username[user]["bonus_credits"] -= 1
#     with open("Userdata.pkl",'wb') as output:
#         pickle.dump(username,output,pickle.HIGHEST_PROTOCOL)
    
def has_credits(user):
    query="select credits from user where email='%s' "%(user)
    pq = ProccessQuery(mysql)
    user_data = pq.process_query(query)
    return user_data[0][0] > 0

@app.route('/new/amazonurl', methods = ['POST'])
def post_amazonurl():
    link = request.form.get('link')
    user = request.form.get('user')
    if has_credits(user):
        r = new_main_caller(link=link,user=user)
        if 'fname' in r:
            print("all went well")
            reduce_credits(user)
            return jsonify(r)
        else:
            print("Oops! somethings went south")
            return jsonify({"error":r})
    else:
        return jsonify({"error":"Not enough credits"})


@app.route('/new/amazonurl/delete', methods = ['POST'])
def post_delete_amazonurl():
    file_name = request.form.get('file')
    user = request.form.get('user')
    import scrapper.ls
    ls = scrapper.ls.linkstore()
    return "\n".join(ls.delete_one_from_table(user,file_name))


@app.route('/new/admin-dashboard/edit', methods = ['POST'])
def post_edit_admin():
    user, referral_count = get_user_by_ref_id(request.form.get('user'))
    if not user is None:
        query = "select bonus_credits from user where email='%s'"%(user)
        pq = ProccessQuery(mysql)
        user_data = pq.process_query(query)
        redeemed_credits = int(request.form.get('value'))
        credits = redeemed_credits + user_data[0][0]
        #username[user][request.form.get('key')]=int(request.form.get('value'))
       # username[user]["credits"]=username[user]["redeemed_credits"]+username[user]["bonus_credits"]
        update_query = "update user set redeemed_credits=%s, credits=%s where email='%s'"%(redeemed_credits,credits,user)
        pq = ProccessQuery(mysql)
        pq.process_query(update_query)
        return jsonify({request.form.get('key'):redeemed_credits,"credits":credits})
    return '0'

@app.route('/new/admin-dashboard/verify', methods = ['POST'])
def post_edit_admin_verify():
    user, referral_count = get_user_by_ref_id(request.form.get('user'))
    if not user is None:
        query = "update user set vericode='verified' where email='%s'"%(user)
        pq = ProccessQuery(mysql)
        pq.process_query(query)
        return '1'
    return '0'


@app.route('/verify', methods = ['POST'])
def post_verify():
    user = request.form.get('Username')
    code = request.form.get('verification')
    print(username)
    query = "select email,vericode from user where email='%s'"%(user)
    pq = ProccessQuery(mysql)
    data = pq.process_query(query)
    if data and data[0][1]!='verified':
        if data[0][1] == code:
            query = "update vericode='verified' from user where email='%s'"%(user)
            pq = ProccessQuery(mysql)
            pq.process_query(query)
            return redirect('/')
    return 'incorrect user or verification code'

@app.route('/redeem', methods = ['POST'])
def post_to_redeem():
    code = request.form.get('voucher')
    user = request.form.get('user')
    value = cm.claim(code)
    credits=0
    bonus_credits =0
    redeemed_credits=0
    referral_id=''
    if value is not None:
        query = "select credits, redeemed_credits, referred_by,bonus_credits,referral_id from user where email ='%s'"%(user)
        pq = ProccessQuery(mysql)
        data = pq.process_query(query)
        if data:
            bonus_credits = data[0][3]
            referral_id=data[0][4]
            credits= data[0][0] +value
            redeemed_credits = data[0][1]+value
            #username[user]["credits"]+=value
            update_query = "update user set credits=%s, redeemed_credits=%s where user='%s'"%(credits,redeemed_credits,user)
            pq = ProccessQuery(mysql)
            pq.process_query(update_query)
            #username[user]["redeemed_credits"]+=value
            if data[0][2]:
                referred_by = data[0][2]
                query = "select bonus_credits, credits from user where email='%s'"%(referred_by)
                pq = ProccessQuery(mysql)
                data = pq.process_query(query)
                if data:
                    ten_percent = int(value*0.2)
                    bonus_credits = data[0][0] + ten_percent
                    referrer_credits= data[0][1] + ten_percent
                    update_query = "update user set credits=%s, bonus_credits=%s where user='%s'"%(referrer_credits,bonus_credits,referred_by)
                    pq = ProccessQuery(mysql)
                    pq.process_query(update_query)
                    #username[referred_by]["bonus_credits"]+=ten_percent
                    #username[referred_by]["credits"]+=ten_percent
#         with open("Userdata.pkl",'wb') as output:
#             pickle.dump(username,output,pickle.HIGHEST_PROTOCOL)
        return main_caller(request.form.get('Link'),request.form.get('user'),password = request.form.get('password'),credits=credits, redeemed = value,bonus=bonus_credits,redeemed_c=redeemed_credits,ref_id=referral_id)
    return '<h1>bad voucher code<h1>'


@app.route('/')
def hello_world():
    return redirect('/new/index')
    #return '<html><body><center><h1>Enter an Amazon Url</h1><br><br><img src="home.jpg" alt="home"><br><br><form action="/target" method="post"><label><h3>Link:</h3><input type="text" name="Link" placeholder = "Enter Link Here" size=100><br><br>Username:<input type="text" name="Username" placeholder = "Enter Username Here" size=30><br>Password:<input type="password" name="password" placeholder = "Please Enter Password Here" size=30></label><button type="submit" value="Submit">Submit</button></form><br><a href="forgot">forgot password?</a><br>You can even Do a Sign-up<a href="/signup"><button type="button" value="Sign-up">SIGNUP</button></a></center></body></html>'
    #return '<html><body><center><h1>Enter your verification code</h1><br><br><br><br><br><br><form action="/verify" method="post"><label><br><br>Username:<input type="text" name="Username" placeholder = "Enter Username Here" size=30><br>verification code:<input type="text" name="verification" placeholder = "Enter verification code Here" size=60></label><button type="submit" value="Submit">Submit</button></form></center></body></html>'
@app.route('/home.jpg')
def home_pic():
    try:
        return send_file(os.getcwd()+'/static/home.jpg', attachment_filename='home.jpg')
    except Exception as e:
        return str(e)

@app.route('/forgot')
def forgotpass():
    return '<html><h1> Welcome to reset password page </h1><br><br><form action="/forgot" method="post"><p>Please submit your username below. You will receive an email with instructions to reset your password</p><input type="hidden" value="11" name="id"><label>Username:<input type="text" name="username" placeholder = "Enter Username Here" size=30><br></label><button type="submit" value="Submit">Reset my password</button></form>'


@app.route('/forgot', methods = ['POST'])
def resetpass():
    id = request.form.get('id')
    user = request.form.get('username')
    query = "select reset_code from user where email='%s'"%(user)
    pq = ProccessQuery(mysql)
    data = pq.process_query(query)
    if id == '11':
        if data:
            reset_code = data[0] 
            if not data[0]:
                reset_code=uuid.uuid4().hex
                update_query= "update user set reset_code ='%s' where email ='%s'"%(reset_code,email)
                pq = ProccessQuery(mysql)
                pq.process_query(update_query)
            mailer.send_reset_mail(user,reset_code)
            return '<html><h1> Welcome to reset password page </h1><br><br><form action="/forgot" method="post"><p>Please complete the form below to reset your password.</p><label><input type="hidden" value="12" name="id">Username:<input type="text" name="username" value='+user+' size=30 readonly><br></label><br><label>reset code: <input type="text" placeholder="Enter reset code" name="reset_code"></label><br><label>new password: <input type="password" placeholder="Enter password" name="password"></label><br><label>Repeat password: <input type="password" placeholder="Enter password" name="password2"></label><br><button type="submit" value="Submit">Reset my password</button></form>'
        else:
            return '<html><h1> Welcome to reset password page </h1><br><br><form action="/forgot" method="post"><p>Please submit your username below. You will receive an email with instructions to reset your password</p><label><input type="hidden" value="11" name="id">Username:<input type="text" name="username" placeholder = "Enter Username Here" size=30><br></label><button type="submit" value="Submit">Reset my password</button></form>'
    elif id == '12':
        #user = request.form.get('username')
        code = request.form.get('reset_code')
        pass_ = request.form.get('password')
        if data:
            if data[0] and data[0][0] == code:
                update_query= "update user set password ='%s' where email ='%s'"%(pass_,email)
                pq = ProccessQuery(mysql)
                pq.process_query(update_query)
#                 username[user]['password']=pass_
#                 with open("Userdata.pkl",'wb') as output:
#                     pickle.dump(username,output,pickle.HIGHEST_PROTOCOL)
                return '<html><p>Password reset successfully. you can <a href="/">login</a> now.</p></html>'
            else:
                return '<html><p>reset code is incorrect. you can <a href="/">go to home</a> now.</p></html>'
        else:
            return '<html><p>user not found. You can <a href="/">go to home</a> now.</p></html>'
    else:
        return '<html><p>invalid form. You can <a href="/">go to home</a> now.</p></html>'

@app.route('/sendverify')
def get_sendverify():
    user = request.args.get('username')
    query="select vericode from user where email='%s'"%(user)
    pq = ProccessQuery(mysql)
    data=pq.process_query(query)
    if data:
        mailer.send(user,data[0][0])
        return 'done'
    return 'user not found'

@app.route('/target',methods = ['POST'])
def get_results():
    print("Here")
    # print(request)
    # print(os.getcwd())
    # print(request.args.get('Link'))
    print(request.form.get('Username'))
    print(request.form.get('password'))
    if request.form.get('Username') in username and (username[request.form.get('Username')]["credits"]+username[request.form.get('Username')]["bonus_credits"])>0 and username[request.form.get('Username')]["password"]==request.form.get('password'):
        if  username[request.form.get('Username')]["vericode"]!='verified':
            return redirect("/verify?username="+request.form.get('Username'))
        if username[request.form.get('Username')]["credits"] == 0:
            username[request.form.get('Username')]["bonus_credits"] = username[request.form.get('Username')]["bonus_credits"] - 1
        else:
            username[request.form.get('Username')]["credits"] = username[request.form.get('Username')]["credits"] - 1
        with open("Userdata.pkl",'wb') as output:
            pickle.dump(username,output,pickle.HIGHEST_PROTOCOL)
        return main_caller(request.form.get('Link'),request.form.get('Username'),password = request.form.get('password'),credits=username[request.form.get('Username')]["credits"],bonus=username[request.form.get('Username')]["bonus_credits"],redeemed_c=username[request.form.get('Username')]["redeemed_credits"],ref_id=username[request.form.get('Username')]["referral_id"])
    else:
        if request.form.get('Username') not in username:
            return "<h1>"+request.form.get('Username')+" Not Found in Our Files. Contact Admin</h1><br><br>"+hello_world()
        else:
            return redirect("/")

# @app.route('/signup',methods = ['GET'])
# def signup():
#     if request.args.get('ref_id') is None:
#         return '<html><h1> Welcome to SignUp Page</h1><br><br><form action="/signup_success" method="post"><label>Username:<input type="text" name="Username" placeholder = "Enter Username Here" size=30><br>Password:<input type="text" name="password" placeholder = "Please Enter Password Here" size=30><br>Referral code:<input type="text" placeholder="enter referral code" name="ref_id"></label><button type="submit" value="Submit">Signup</button></form>'
#     else:
#         return '<html><h1> Welcome to SignUp Page</h1><br><br><form action="/signup_success" method="post"><label>Username:<input type="text" name="Username" placeholder = "Enter Username Here" size=30><br>Password:<input type="text" name="password" placeholder = "Please Enter Password Here" size=30><br>Referral code:<input type="text" value="'+request.args.get('ref_id')+'" name="ref_id"></label><button type="submit" value="Submit">Signup</button></form>'


# @app.route('/signup_success',methods = ['POST'])
# def signup_success():
#     if request.form.get('Username') not in username:
#         username[request.form.get('Username')] = dict()
#         username[request.form.get('Username')]["password"] = request.form.get('password')
#         username[request.form.get('Username')]["credits"] = 50
#         username[request.form.get('Username')]["referral_id"] = uuid.uuid4().hex[0:5]
#         username[request.form.get('Username')]["bonus_credits"]=0
#         username[request.form.get('Username')]["referral_count"]=0
#         username[request.form.get('Username')]["redeemed_credits"]=0
#         vericode = uuid.uuid4().hex
#         username[request.form.get('Username')]["vericode"] = vericode
#         if not request.form.get('ref_id') is None:
#             referred_by = get_user_by_ref_id(request.form.get('ref_id'))
#             if not referred_by is None:
#                 username[request.form.get('Username')]["referred_by"] = referred_by
#         with open("Userdata.pkl",'wb') as output:
#             pickle.dump(username,output,pickle.HIGHEST_PROTOCOL)
#         mailer.send(request.form.get('Username'),vericode)
#         return redirect("verify?username="+request.form.get('Username'))
#         #return '<html><body><h1>Username: '+ request.form.get('Username') + '<br>Password: '+request.form.get('password')+'<br>Credits Available are: '+str(username[request.form.get('Username')]["credits"])+"</h1>"+hello_world()
#     else:
#         return '<h1>Username Already Exists.<br><br>'+signup()


def get_user_by_ref_id(ref_id):
    #print("ref_id::"+str(ref_id))
    query = "select email,referral_count from user where referral_id='%s'"%(ref_id)
    pq = ProccessQuery(mysql)
    result = pq.process_query(query)
    #print("Result::"+str(result))
    if result:
        return (result[0][0], result[0][1])
    else:
        return None
#     for key in username.keys():
#         if "referral_id" in username[key].keys():
#             if username[key]["referral_id"] == ref_id:
#                 return key

@app.route('/get_file',methods = ['GET'])
def get_file():
    print(request.args.get('filename'))
    return send_from_directory('scrapper',request.args.get('filename'),as_attachment=True)


@app.route('/update_data',methods = ['POST'])
def update_credits():
    # results = username
    print(request.form.get("username") ,"    " ,request.form.get("password"))
    if request.form.get("username")=="admin" and request.form.get("password")=="!C20180524n":
        return render_template('result.html',result = username, vouchers = cm.vouchers())
    else:
        return "<h1> Credentials Are Wrong</h1>"

@app.route('/update_details',methods = ['GET'])
def update_details():
    unames = request.args.getlist("uname")
    # print(unames)
    credits = request.args.getlist("credits")
    verifieds = request.args.getlist("verified")
    # print(credits)
    for x in range(0,len(unames)):
        credits = int(credits[x])
        if int(verifieds[x]) == 1:
            query = "update user set credits='%s', vericode='verified' where email='%s'"%(credits,unames[x])
        else:
            query = "update user set credits='%s' where email='%s'"%(credits,unames[x])
        pq = ProccessQuery(mysql)
        pq.process_query(query)
            #username[unames[x]]["vericode"] = 'verified'
    #return str(username)
    return render_template('result.html',result = username, vouchers = cm.vouchers())

@app.route('/admin',methods = ['GET'])
def admin():
    return redirect('/new/admin')
    # return '<html><h1> Welcome to ADMIN LOGIN Page</h1><br><br><form action="/update_data" method="post"><label>Username:<input type="text" name="username" placeholder = "Enter Username Here" size=30><br>Password:<input type="text" name="password" placeholder = "Please Enter Password Here" size=30></label><button type="submit" value="Submit">Signup</button></form>'
    # return render_template('result.html',result = username)


if __name__ == '__main__':
    app.run()
