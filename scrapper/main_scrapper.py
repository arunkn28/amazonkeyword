import pandas as pd
import requests
from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor
import time
from time import strftime, gmtime
import pandas as pd
import os
os.chdir(os.path.dirname(os.path.dirname(__file__)))
headers = {'content-type': 'application/json',
           'Connection':'Keep-alive',
           'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Referer': '',
            'Accept-Language': 'en-US,en;q=0.9',
           'DNT':'1'
           }


def scrap_titles(link):
    headers["Referer"] = link
    r = requests.get(link,headers= headers)
    soup = BeautifulSoup(r.text, 'html.parser')
    # print(soup)
    results = soup.find_all('h2', {'class': 'a-size-medium s-inline s-access-title a-text-normal'})
    # print(len(results))
    if len(results)==0:
        results = soup.find_all('h2', {'class': 'a-size-base s-inline s-access-title a-text-normal'})
    # print(len(results))
    return [result.text for result in results]


def main_caller(link=None,username=None,password=None,credits = None,no_of_pages = 20,redeemed = 0,redeemed_c=0,bonus=0,ref_id = None):
    final_result = []
    assert link is not None,"Link is None. Please Check"
    links = [link+"&page="+str(x) for x in range(1,no_of_pages+1)]
    with ThreadPoolExecutor(max_workers=10) as executor:
        # Start the load operations and mark each future with its URL
        future_to_url = [executor.submit(scrap_titles, link) for link in links]
        for x in future_to_url:
            url = x
            try:
                final_result += x.result()
            except Exception as exc:
                print('%r generated an exception: %s' % (url, exc))

    main_string = ""
    for x in final_result:
        main_string = main_string + " " + x
    lis_to_remove =['[Sponsored]','Sponsored','&','-','+',']','[','(',')','*',':']
    for x in lis_to_remove:
        main_string = main_string.replace(x,"")

    word_count = dict()
    word_split = main_string.split(" ")
    for x in word_split:
        if x in word_count.keys():
            word_count[x] += 1
        else:
            word_count[x] = 1
    file_name = "csv_file"+strftime('%H_%M_%S', gmtime())+".csv"
    df = pd.DataFrame(columns=["Word","Count"])
    for x in word_count.keys():
        try:
            df = df.append({'Word':x,"Count":word_count[x]},ignore_index=True)
        except:
            continue
    df = df.sort_values(by=["Count"],ascending=False)
    print(os.getcwd())
    df.reset_index(inplace=True)
    df.to_csv("scrapper/"+file_name,index=False)
    if redeemed > 0:
        return '<html><head><style>table.dataframe {table-layout: fixed;width: 350px;    }</style></head><body><h1>Results. You Have '+str(credits)+' Credits Remaining. </h1><br><br><p>Referral id: '+ref_id+' <br>Redeemed credits: '+str(redeemed_c)+' <br> Bonus credits: '+str(bonus)+' <br><small>right click <a href="/signup?ref_id='+ref_id+'">here</a> to copy referral link</small></p><br><br><br><br><form action="/target" method="post"><label>Link:<input type="text" size="300" name="Link" id ="link" value = '+link+'></label><input type="hidden" name="Username" value='+username+'><input type="hidden" name="password" value='+password+'></label><button type="submit" value="Submit">Submit</button></form><button onclick="document.getElementById('+"'link'"+').value ='+"''"+'">RESET</button> <br><form method="POST" action="redeem"><label for="voucher">Enter Voucher code to redeem</label><input type="text" size="50" id="voucher" name="voucher"><input type="hidden" id="user" name="user" value="'+username+'"><input type="hidden" name="password" value='+password+'><input type="hidden" size="300" name="Link" id ="link" value = '+link+'><br><input type="submit"><p><small>you have redeemed '+str(redeemed)+' points</p></small></form><br><br><br></body></html><table><tr><td></td><td><form action="/get_file"><input type="hidden" value="'+file_name+'" name="filename"><button type="submit">Download CSV File</button><form></td></tr><td valign="top"><textarea rows="40" cols="80">'+"\n".join(final_result)+"</textarea></td><td>"+df.to_html(columns=["Word","Count"],index=False)+"</td>"
    else:
        return '<html><head><style>table.dataframe {table-layout: fixed;width: 350px;    }</style></head><body><h1>Results. You Have '+str(credits)+' Credits Remaining. </h1><br><br><p>Referral id: '+ref_id+' <br>Redeemed credits: '+str(redeemed_c)+' <br> Bonus credits: '+str(bonus)+' <br><small>right click <a href="/signup?ref_id='+ref_id+'">here</a> to copy referral link</small></p><br><br><br><br><form action="/target" method="post"><label>Link:<input type="text" size="300" name="Link" id ="link" value = '+link+'></label><input type="hidden" name="Username" value='+username+'><input type="hidden" name="password" value='+password+'></label><button type="submit" value="Submit">Submit</button></form><button onclick="document.getElementById('+"'link'"+').value ='+"''"+'">RESET</button> <br><form method="POST" action="redeem"><label for="voucher">Enter Voucher code to redeem</label><input type="text" size="50" id="voucher" name="voucher"><input type="hidden" id="user" name="user" value="'+username+'"><input type="hidden" name="password" value='+password+'><input type="hidden" size="300" name="Link" id ="link" value = '+link+'><br><input type="submit"></form><br><br><br></body></html><table><tr><td></td><td><form action="/get_file"><input type="hidden" value="'+file_name+'" name="filename"><button type="submit">Download CSV File</button><form></td></tr><td valign="top"><textarea rows="40" cols="80">'+"\n".join(final_result)+"</textarea></td><td>"+df.to_html(columns=["Word","Count"],index=False)+"</td>"

def new_main_caller(link=None,no_of_pages = 20,user=None):
    final_result = []
    assert link is not None,"Link is None. Please Check"
    links = [link+"&page="+str(x) for x in range(1,no_of_pages+1)]
    with ThreadPoolExecutor(max_workers=10) as executor:
        # Start the load operations and mark each future with its URL
        future_to_url = [executor.submit(scrap_titles, link) for link in links]
        for x in future_to_url:
            url = x
            try:
                final_result += x.result()
            except Exception as exc:
                print('%r generated an exception: %s' % (url, exc))
                return '%r generated an exception: %s' % (url, exc)

    main_string = ""
    for x in final_result:
        main_string = main_string + " " + x
    lis_to_remove =['[Sponsored]','Sponsored','&','-','+',']','[','(',')','*',':']
    for x in lis_to_remove:
        main_string = main_string.replace(x,"")

    word_count = dict()
    word_split = main_string.split(" ")
    for x in word_split:
        if x in word_count.keys():
            word_count[x] += 1
        else:
            word_count[x] = 1
    file_name = "csv_file"+strftime('%H_%M_%S', gmtime())+".csv"
    df = pd.DataFrame(columns=["Word","Count"])
    for x in word_count.keys():
        try:
            df = df.append({'Word':x,"Count":word_count[x]},ignore_index=True)
        except:
            continue
    df = df.sort_values(by=["Count"],ascending=False)
    #print(df)
    df.reset_index(inplace=True)
    df.to_csv("scrapper/"+file_name,index=False,encoding='utf-8-sig')
    from .linkstore import linkstore
    ls = linkstore()
    if user is not None and link is not None:
        ls.insert_in_table(user,link,file_name)
    print(df)
    return {"final_result": "\n".join(final_result),"in_html":df.to_html(columns=["Word","Count"],index=False),"fname":file_name}
# if __name__ == '__main__':
#     start = time.time()
    #     final_result = main_caller(link = "https://www.amazon.it/s/ref=nb_sb_noss_1/257-1781845-4353651?__mk_it_IT=%C3%85M%C3%85%C5%BD%C3%95%C3%91&url=search-alias%3Daps&field-keywords=gopro",no_of_pages = 5)
#     end = time.time()
#     print("Total Time Taken is : ",end-start)
