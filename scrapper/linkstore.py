import sqlite3
class linkstore:
    def __init__(self):
        DATABASE_NAME = 'linksDB'
        self.connection = sqlite3.connect('linksDB')
        self.crsr = self.connection.cursor()
        self.create_table()

    def create_table(self):
        self.crsr.execute("SELECT name FROM sqlite_master WHERE type='table';")
        if(len(self.crsr.fetchall()) < 1):
            #create table records_table
            sql_command = """CREATE TABLE IF NOT EXISTS links_table (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            user VARCHAR(40),
            link VARCHAR(100),
            json_file VARCHAR(20));"""
            self.crsr.execute(sql_command)
            self.connection.commit()

    def insert_in_table(self,user,link,js):
        sql_command ="INSERT INTO links_table (user,link,json_file) VALUES ('"+user+"','"+link+"','"+js+"');"
        self.crsr.execute(sql_command)
        self.connection.commit()

    def get_all_from_table(self,user):
        sql_command ="SELECT * FROM links_table;"
        self.crsr.execute(sql_command)
        data = self.crsr.fetchall()
        for i in range(len(data)):
            data[i] =list(data[i])
        self.connection.commit()
        return data

    def get_one_from_table(self,user):
        sql_command ="SELECT * FROM links_table WHERE user='"+user+"';"
        self.crsr.execute(sql_command)
        data = self.crsr.fetchall()
        for i in range(len(data)):
            data[i] =list(data[i])
        self.connection.commit()
        return data
    def delete_one_from_table(self,user,file_name):
        sql_command ="DELETE FROM links_table WHERE user='"+user+"' AND json_file='"+file_name+"';"
        self.crsr.execute(sql_command)
        data = self.crsr.fetchall()
        self.connection.commit()
        return data
