import uuid
import pickle
from query_processor import ProccessQuery
# try:
#     with open('creditstore.pkl', 'rb') as input:
#         store = pickle.load(input)
# except:
#     store = {"default":50}


class CM:

    def __init__(self,mysql):
        self.mysql=mysql
        query="select * from creditstore"
        pq = ProccessQuery(self.mysql)
        data = pq.process_query(query)
        n = 20
        if data:
            n = 20 - len(data)
        print("N value::"+str(n))
        for i in range(n):
            value = (i%4+1)*10
            voucher = uuid.uuid4().hex
            #store[voucher]=value
            insert_query="insert into creditstore(voucher,value) values('%s',%s)"%(voucher,value)
            pq = ProccessQuery(self.mysql)
            pq.process_query(insert_query)

    def claim(self,code):
        #value = store.pop(code,None)
        query="select value from creditstore where voucher='%s'"%(code)
        pq = ProccessQuery(self.mysql)
        data = pq.process_query(query)
        if data is None:
            value =None
            print('unknown voucher code')
        else:
            value=data[0][0]
            print('voucher claimed: '+code+' value: '+str(value))
            delete_query = "delete from creditstore where voucher='%s'"%(code)
            pq = ProccessQuery(self.mysql)
            pq.process_query(delete_query)
#             with open("creditstore.pkl",'wb') as output:
#                 pickle.dump(store,output,pickle.HIGHEST_PROTOCOL)
        return value
# 
#     def printvouchers(self):
#         print(store)
#          	
    def vouchers(self):
        query="select voucher,value from creditstore"
        pq = ProccessQuery(self.mysql)
        data = pq.process_query(query)
        store={}
        for i in range(len(data)):
            store[data[i][0]]=data[i][1]
        return store
  