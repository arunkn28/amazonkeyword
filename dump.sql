 CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `vericode` varchar(50) DEFAULT NULL,
  `bonus_credits_growth` int(11) DEFAULT '0',
  `redeemed_credits_growth` int(11) DEFAULT '0',
  `redeemed_credits` int(11) DEFAULT '0',
  `credits` int(11) DEFAULT '0',
  `bonus_credits` int(11) DEFAULT '0',
  `referral_count_growth` int(11) DEFAULT '0',
  `referral_count` int(11) DEFAULT '0',
  `referral_id` varchar(50) DEFAULT NULL,
  `referred_by` varchar(50) DEFAULT NULL,
  `reset_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`)
);

CREATE TABLE `creditstore` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `voucher` varchar(50) DEFAULT NULL,
  `value` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
);